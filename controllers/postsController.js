
var express = require('express');
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');
var q = require('q');
var d = q.defer();
var cloudinary = require('cloudinary');
var async = require('async');

cloudinary.config({
	cloud_name: 'dxnapa5zf',
	api_key: '219348637198157',
	api_secret: 'FJl9rCE5dTS_kgkX_rPvUyMTwZY'
});
var postsController = function(postsModel, userModel){
    function isEqualId(element) {
        return element >= 15;
      }
    var get = function(req,res){

        postsModel.getlistpost()
        .then(function (result) {
            
           res.status(200).json(result);
        })
        .catch(function (error) {
            console.log(error);
            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
        })

    }
    
 

    var getDetail = function(req, res){

      var id=req.params.postsid;
      postsModel.getdetailpost(id)
        .then(function (result) {
            res.status(200).json(result);
        })
        .catch(function (error) {
            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
        })


    }
    var createposts = function(req, res)
    {
        var lstErr = [];
        var lstMessErr = [];
        var model = req.body;
        if(!req.auth){
            res.status(403).send('Bạn chưa đăng nhập. Vui lòng đăng nhập');
        }else{

            if(!model.user_id)
            {
                lstMessErr.push('userid khong duoc de trong');
            }
            if(!model.city_id)
            {
                lstMessErr.push('city_id khong duoc de trong');
            }
            if(!model.district_id)
            {
                lstMessErr.push('district_id khong duoc de trong');
            }
            if(!model.room_type_id)
            {
                lstMessErr.push('room_type_id khong duoc de trong');
            }
            if(lstMessErr.length >0){
                res.status(404).json({errors: lstMessErr});
            }
    
            else
            {
                
                var newposts={
                    user_id:model.user_id,
                    title:model.title,
                    description:model.description,
                    city_id:model.city_id,
                    district_id:model.district_id,
                    room_type_id:model.room_type_id,
                    address:model.address,
                    price:model.price,
                    price2:model.price2,
                    acreage:model.acreage,
                    create_at:new Date(Date.now()).toISOString(),
                    modify_at:new Date(Date.now()).toISOString(),
                    longtitude:model.longtitude,
                    latitude:model.latitude,
                    count_vote : 0,
                    total_vote_value: 0,
                    status:0, // trạng thái cho biết phòng này đã được đặt chưa
                    duyet_bai: 0, // Bài post này đã được duyệt chưa
                    isDelete: 0 // Bài post này còn tồn tại ko
                }
              
                postsModel.createposts(newposts)
                .then( data =>{
                   var idInsert =  data.insertId;
                   if (req.files){
                         let filePaths = req.files;
                         let upload_len = filePaths.length
                         ,upload_res = new Array();
                        var insertUploadPromise  = new Promise((resolve, reject) => {
                            const tasks = filePaths.map(element => callback => { 
                                let filePath = element;
                                cloudinary.v2.uploader.upload(filePath.path, (error, result) => {
                                
                                if(result)
                                {
                                    var newImage = {
                                        "post_id" : idInsert,
                                        "url" : result.url
                                    }
                                    postsModel.insertImageForPost(newImage).then(data=>{
                                        callback(null, data)
                                    }).catch(err =>{
                                        callback(err)
                                    })
                                } else if(error) {
                                    callback(error)
                                }
                            })
    
                        })
                        async.parallel(tasks, (err, result) => {
                            if (err) {
                              reject(err);
                            }
                            else resolve(result);
                        })
                    })
                    .then(
                        result => {
                            res.status(200).json(result);
                        },
                        error => {
                            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                        }
                    )
                  }else{
                    res.status(200).json("Success");
                  }
                })
                .catch(errors =>{
                    res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                })
            }

        }
        

    }
    var duyetPost = function(req, res){
        var postId = req.params.postId;
        postsModel.duyetPost(postId)
        .then(function (result) {
            if(result.changedRows <1)
            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
            else
            res.status(200).json("Duyệt post thành công");
        })
        .catch(function (error) {
            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
        })
    }
     
    var updatepost =function(req, res)
    {
        var lstErr = [];
        var lstMessErr = [];
        var model = req.body;
        if(!req.auth){
            res.status(403).send('Bạn chưa đăng nhập. Vui lòng đăng nhập');
        }
        else{
            if(!model.id)
            {
                lstMessErr.push('id khong duoc de trong');
            }
            if(!model.user_id)
            {
                lstMessErr.push('userid khong duoc de trong');
            }
            if(!model.city_id)
            {
                lstMessErr.push('city_id khong duoc de trong');
            }
            if(!model.district_id)
            {
                lstMessErr.push('district_id khong duoc de trong');
            }
            if(!model.room_type_id)
            {
                lstMessErr.push('room_type_id khong duoc de trong');
            }
            if(lstMessErr.length >0){
                res.status(404).json({errors: lstMessErr});
            }
            else
            {
                var newposts={
                    id:model.id,
                    user_id:model.user_id,
                    title:model.title,
                    description:model.description,
                    city_id:model.city_id,
                    district_id:model.district_id,
                    address:model.address,
                    price:model.price,
                    price2:model.price2,
                    acreage:model.acreage,
                    create_at:new Date(Date.now()).toISOString(),
                    modify_at:new Date(Date.now()).toISOString(),
                    longtitude:model.longtitude,
                    latitude:model.latitude,
                    count_vote:model.count_vote,
                    total_vote_value:model.total_vote_value,
                    status:model.status,
                    room_type_id:model.room_type_id,
    
                }
    
                
                postsModel.updateposts(newposts)
                .then( data =>{
                   var idImage =  newposts.id;
                   if (req.files){
                       
                         let filePaths = req.files;
                         let upload_len = filePaths.length
                         ,upload_res = new Array();
                        var insertUploadPromise  = new Promise((resolve, reject) => {
                            const tasks = filePaths.map(element => callback => { 
                                let filePath = element;
                                cloudinary.v2.uploader.upload(filePath.path, (error, result) => {
                                
                                if(result)
                                {
                                    var newImage = {
                                        "post_id" : idImage,
                                        "url" : result.url
                                    }
                                    postsModel.updateImageForPost(newImage).then(data=>{
                                        callback(null, data)
                                    }).catch(err =>{
                                        callback(err)
                                    })
                                } else if(error) {
                                    callback(error)
                                }
                            })
    
                        })
                        async.parallel(tasks, (err, result) => {
                            if (err) {
                              reject(err);
                            }
                            else resolve(result);
                        })
                    })
                    .then(
                        result => {
                            res.status(200).json(result);
                        },
                        error => {
                            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                        }
                    )
                    }else{
                        res.status(200).json("Success");
                    }
                })
                .catch(errors =>{
                    res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                })
            }
               

        }
        
    
    }
    var deleteposts=function(req,res)
    {
        if(!req.auth){
            res.status(403).send('Bạn chưa đăng nhập. Vui lòng đăng nhập');
        }
        else
        {    
             var postsid=req.params.postsid;
             postsModel.deleteposts(postsid)
            .then(function (result) {
                res.status(200).json(result);
            })
            .catch(function (error) {
                res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
            })
         }
    }
    var updateStatus =function(req, res)
    {
        var lstErr = [];
        var lstMessErr = [];
        var model = req.body;
        if(!model.postId)
        {
            lstMessErr.push('id1 khong duoc de trong');
        }
        if(model.status<0 || model.status>1)
        {
            lstMessErr.push('status không hợp le');
        }
   
    
    
    
        if(lstMessErr.length >0){
            res.status(404).json({errors: lstMessErr});
        }
        else
        {
            var post={
                postId:model.postId,
                status:model.status
            //    modify_at:new Date(Date.now()).toISOString()

            }
            postsModel.updateStatus(post)
            .then(function (result) {
                res.status(200).json("cap nhat bai  dang thành công");
            })
            .catch(function (error) {
                console.log(error);
                res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
            })
           }
           
    
    }
    var getPostByUser = function(req, res){
        if(!req.auth){
            res.status(403).send('Bạn chưa đăng nhập. Vui lòng đăng nhập');
        }else{
              var userId= req.auth.userId;
              postsModel.getPostByUser(userId)
                .then(function (result) {
                    res.status(200).json(result);
                })
                .catch(function (error) {
                    console.log(error);
                    res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                })
        }
    }
    var searchByClause = function(req, res){
        var model = req.body;
        var whereClause = "WHERE ";
        whereClause += (whereClause == "WHERE "?"":"AND ");
        // model ={
        //     "district":1 ,
        //     "address" :"Nguyen Van Cu" ,
        //     "city": 1,
        //     "price": 100,
        //     "room_type" : 1
        // };
        if(model.city){
            whereClause += (whereClause == "WHERE "?"":"AND ");
            whereClause += "p.city_id = "+ model.city + " ";
        }
        
        if(model.district){
            whereClause += (whereClause == "WHERE "?"":"AND ");
            whereClause += "p.district_id  = "+ model.district + " ";
        }
        if(model.room_type){
            whereClause += (whereClause == "WHERE "?"":"AND ");
            whereClause += "p.room_type_id  = "+ model.room_type + " ";
        }
        if(model.address){
            whereClause += (whereClause == "WHERE "?"":"AND ");
            whereClause += "p.address like '"+ model.address + "' ";
        }
        
        if(model.price){
            whereClause += (whereClause == "WHERE "?"":"AND ");
            whereClause += "p.price  <= "+ model.price + " ";
        }
        whereClause += " AND p.isDelete= 0 and  duyet_bai =1";
        
        postsModel.searchByClause(whereClause)
        .then(function (result) {
            res.status(200).json(result);
        })
        .catch(function (error) {
            console.log(error);
            res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
        })
    }
    var getCensorship = function(req, res){
        
        if(!req.auth){
            res.status(403).send('Bạn chưa đăng nhập. Vui lòng đăng nhập');
        }else{
            
            if(req.auth.role !="admin"){
                res.status(403).send('Không có quyền truy cập');
            }else{
                postsModel.getCensorship()
                .then(function (result) {
               
                     res.status(200).json(result);
                })
                .catch(function (error) {
                    res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
                    
                })

            }
        }
    }
    return {
        get:get,
        
        getDetail: getDetail,
        createposts:createposts,
        updatepost:updatepost,
        deleteposts:deleteposts,
        duyetPost:duyetPost,
        updateStatus:updateStatus,
        getPostByUser: getPostByUser,
        searchByClause: searchByClause,
        getCensorship: getCensorship
    }

}
module.exports = postsController;
