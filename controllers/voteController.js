
var q = require('q');
var d = q.defer();
var voteController = function(voteModel, userModel, postsModel){
    
        var getListVote = function(req,res){
            voteModel.getListVote()
            .then(function (data) {
                res.status(200).json(data);
            })
            .catch(function (error) {
                res.status(404).json({errors: ['Có lỗi trong quá trình xử lý']});
            })
        }
        var createVote = function(req,res){
            var model = req.body;
            req.checkBody('user_id', 'Người dùng vote không được để trống').notEmpty();
            req.checkBody('posts_id', 'Bài post vote không được để trống').notEmpty();
            req.checkBody('vote', 'Điểm vote phải la 1 số number').isInt();
            var errors = req.validationErrors();
    
            if(errors.length >0){
                res.status(404).json({errors: errors});
            }else{
                //Vote: Them 1 dong vao bang vote. 
                //Dua vao post_id va user nguoi thue vote cho bai post cua user nguoi cho thue->
                //Thu 1:  cap nhat diem tin cay cho nmguoi cho thue tang len cong thuc: Điểm uy tín cộng thêm = (trung bình số sao của bài post x 0.2)
                //Trung binh so sao = tong diem sao/ tong nguoi vote
                //Thu 2: Cap nhat tong so luong sao va tong nguoi vote trong table posts
                var vote = {
                    user_id : model.user_id,
                    posts_id: model.posts_id,
                    vote: model.vote,
                    created_at:new Date(Date.now()).toISOString(), 
                    updated_at: new Date(Date.now()).toISOString()
                }  
                postsModel.getdetailpost( model.posts_id).then(result =>{
                    var post = result[0];
                    var createVote =  voteModel.createVote(vote);
                    var updated_at = new Date(Date.now()).toISOString();
                    var idPost = model.posts_id;
                    var tbPre = post.total_vote_value /  post.count_vote;// trung binh so sao vote truoc fo
                    post.count_vote = post.count_vote +1;
                    post.total_vote_value =post.total_vote_value + vote.vote ; 
                    var updateCountVoteAndTotalVote =postsModel.updateCountVoteAndTotalVote(post);
                    var tbCurrent = post.total_vote_value /  post.count_vote; // trung binh so sao vote  khi user nay vote
                    var scores=  (tbCurrent - tbPre)* 0.2; //diem uy tinh cong them hoac tru di vao diem uy tin truoc do

                    var updateScores = userModel.updateScores(scores, updated_at, idPost);
                    
                    q.all([createVote,updateScores, updateCountVoteAndTotalVote])
                    .then(result =>{
                        res.status(200).json("Insert vote success!");
                    })
                    .catch(errors =>{
                        res.status(404).json({errors: errors});
                    })
                  
                })
                .catch(errors =>{
                    res.status(404).json({errors: errors});
                })
                
            }
               
        }
       
        return {
            getListVote: getListVote,
            createVote: createVote
        }
        
    }
    module.exports = voteController;