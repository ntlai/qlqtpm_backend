var mysql = require('mysql'),
q = require('q');
db = require('../fn/mysql-db');

var d = q.defer();
var postsModel= {
	getlistpost: function(){
			var sql='SELECT  `posts`.`id`, `image`.`url`,`posts`.`title`,`posts`.`price`, `user`.`username`, `posts`.`longtitude`,`posts`.`latitude`, `posts`.`price2` from `posts`,`image`,`user` where `posts`.`isDelete` = 0  and   `posts`.`duyet_bai` = 1 and  `posts`.`user_id` = `user`.`id` and `image`.`post_id`= `posts`.`id` and `posts`.`isDelete` =0 and `posts`.`status` = 1 group by `posts`.`id`';
			return db.load(sql);
	},
	getdetailpost: function(id){
		var sql= 'select `user`.`username`,`posts`.`title`,`posts`.`description`,`posts`.`address`,`posts`.`price`,`posts`.`acreage`,`posts`.`create_at`,`posts`.`modify_at`,`posts`.`longtitude`,`posts`.`latitude`,`posts`.`count_vote`,`posts`.`total_vote_value`,`image`.`url`,`district`.`name` as districtname,`city`.`name` as cityname,`room_type`.`name` as roomtypename from `posts`,`city`,`district`,`image`,`room_type`,`user` where  `posts`.`isDelete` = 0  and   `posts`.`duyet_bai` = 1 and  `posts`.`city_id` =`city`.`id` and `posts`.`district_id`= `district`.`id`	and `posts`.`room_type_id`=`room_type`.`id` and  `posts`.`id`=`image`.`post_id` and `posts`.`user_id`=`user`.`id`  and `posts`.`id`='+id;
		return db.load(sql);
	},
	createposts: (newposts)=>{
		var  sql = `insert into posts values(null, ${newposts.user_id}, '${newposts.title}','${newposts.description}', 
		${newposts.city_id}, ${newposts.district_id}, ${newposts.room_type_id} , '${newposts.address}' , 
		${newposts.price}, ${newposts.price2}, ${newposts.acreage},
		'${newposts.create_at}', '${newposts.modify_at}' , ${newposts.longtitude}, ${newposts.latitude},
		${newposts.count_vote},${newposts.total_vote_value},
		${newposts.status},${newposts.duyet_bai} ,${newposts.isDelete})`;

		return db.load(sql);

	},
	insertImageForPost: (newImage) =>{
		var  sql = `insert into image values(null, ${newImage.post_id}, '${newImage.url}')`;
		return db.load(sql);
	},
	updateImageForPost: (newImage) =>{
		var  sql = `update image set url='${newImage.url}' where post_id=${newImage.post_id}`;
		return db.load(sql);
	},
	updateposts: (newposts)=>{
		var  sql = `update posts set  title= '${newposts.title}',description='${newposts.description},city_id =${newposts.city_id},
		district_id=${newposts.district_id},address=${newposts.address},price=${newposts.price}, price2=${newposts.price2},acreage=${newposts.acreage},longtitude=${newposts.longtitude},
		latitude=${newposts.latitude},count_vote=${newposts.count_vote},total_vote_value=${newposts.total_vote_value},status=${newposts.status},room_type_id=${newposts.room_type_id}
		where id=${newposts.id}`;
		return db.load(sql);
	},
	deleteposts:(postsid)=>{
		var sql='UPDATE `order`,`posts` SET `posts`.`isDelete`= 0 and `order`.`status`=0 WHERE `posts`.`id`='+postsid+' and `order`.`posts_id`=`posts`.`id`';
		return db.load(sql);
	},
	duyetPost : (postId)=>{
		var sql ='UPDATE `posts` SET `duyet_bai` = 1  WHERE `posts`.`id` = '+postId;
		// var sql ='UPDATE ';
		//console.log(sql);
		return db.load(sql);
	},
	updateStatus:(post)=>{
		var sql='UPDATE `posts` SET `posts`.`status`= ' +post.status+ ' WHERE `posts`.`id`='+post.postId;
		return db.load(sql);
	},
	updateCountVoteAndTotalVote: (post)=>{
		var  sql = `update posts set  count_vote = 
		${post.count_vote}, total_vote_value=  ${post.total_vote_value} where id=  ${post.id} `;
		return db.load(sql);
	},
	getPostByUser: (idUser)=>{
		var sql =`select* from posts where isDelete= 0 and   user_id = ${idUser}`;
		return db.load(sql);
	},
	getCensorship: ()=>{
		var sql =`select* from posts where isDelete= 0 and  duyet_bai =0`;
		return db.load(sql);
	},
	searchByClause: (whereClause)=>{
		var sql =`select* from posts p ${whereClause}`;
		console.log(sql);
		return db.load(sql);
	}
	

}
module.exports = postsModel;