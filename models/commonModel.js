var mysql = require('mysql'),
q = require('q');
db = require('../fn/mysql-db');

var d = q.defer();

var commonModel= {
    getListDistrict :  function() {
        var  sql = 'select* from district';
        //console.log('list user');
        return db.load(sql);
    },
    getListAcreage :  function() {
        var  sql = 'select* from acreage';
        //console.log('list user');
        return db.load(sql);
    },
    getListCity :  function() {
        var  sql = 'select* from city';
        //console.log('list user');
        return db.load(sql);
    },
    getListPrice :  function() {
        var  sql = 'select* from price';
        //console.log('list user');
        return db.load(sql);
    }
 }
module.exports = commonModel;