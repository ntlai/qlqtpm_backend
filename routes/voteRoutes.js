var express = require('express');

var routes = function(voteModel, userModel, postsModel){
   var voteRouter = express.Router();
   var voteController = require('../controllers/voteController')(voteModel, userModel, postsModel);
   voteRouter.route('/')
    .get(voteController.getListVote)
    voteRouter.route('/createVote')
    .post(voteController.createVote)
    return voteRouter;
};
module.exports = routes;
