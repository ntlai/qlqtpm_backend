var express = require('express');
var multer = require('multer');
var crypto = require('crypto');
var mime = require('mime');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './public/')
    },
    filename: function (req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
      });
    }
  });

    
var upload = multer({ storage: storage });
var routes = function(postsModel, userModel){
   var userRouter = express.Router();
   var postsController = require('../controllers/postsController')(postsModel, userModel);
   userRouter.route('/')
    .get(postsController.get);
   userRouter.route('/getPostByUser')
    .get(postsController.getPostByUser)
    userRouter.route('/getCensorship')
    .get(postsController.getCensorship)
    userRouter.route('/searchByClause')
    .post(postsController.searchByClause);
   //middleware
   userRouter.route('/:postsid')
    .get(postsController.getDetail)
    userRouter.route('/update')
   .put(postsController.updatepost);
    userRouter.route('/create')
    //.post(postsController.createposts);
    .post(upload.array('images', 10), postsController.createposts)
     userRouter.route('/delete/:postsid')
    .put(postsController.deleteposts);
    userRouter.route('/duyetPost/:postId')
    .put(postsController.duyetPost);
    userRouter.route('/updateStatus')
    .put(postsController.updateStatus);
    
    
    return userRouter;
  
};
module.exports = routes;